////////////////////////////////////////////
/////cpp vector intialztion/////////////////
/////made by alex///////////////////////////
////////////////////////////////////////////
#include "Vector.h"
#include <iostream>
#define MIN_CAPACITY 2
#define DEFAULT_RESIZE_FACTOR 5
#define ERROR -9999
using std::string;
using std::cout;
using std::endl;
Vector::Vector(int n)//constructor
{
	if (n < MIN_CAPACITY)
	{
		n = MIN_CAPACITY;
	}
	this->_elements = new int[n];
	this->_capacity = n;
	this->_size = 0;
	this->_resizeFactor = DEFAULT_RESIZE_FACTOR;
}
Vector::~Vector()//deconstructor
{
	delete[] this->_elements;
	cout << "d'tor test" << endl;
}
int Vector::size() const//this function return Vector's size
{
	return this->_size;
}
int Vector::capacity() const//this function retuns the vector capacity
{
	return this->_capacity;
}
int Vector::resizeFactor() const//this function retuns the vector resize factor
{
	return this->_resizeFactor;
}
bool Vector::empty() const//this function return true if the vector is empty and false if not empty
{
	if (this->_size == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
this function adds one number to the end of the vector
---------------
input
---------------
vlaue to add
---------------
output
---------------
none
*/
void Vector::push_back(const int& val)
{
	int *newElements;
	if (this->_size < (this->_capacity))
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else
	{
		newElements = new int[this->_capacity + this->_resizeFactor];
		for (int i = 0; i < this->_capacity; i++)//copying elements in old vector
		{
			newElements[i] = this->_elements[i];
		}
		this->_capacity += this->_resizeFactor;
		this->_elements[this->_size] = val;
		this->_size++;
		delete[] this->_elements;//deleting old vector
		this->_elements = newElements;
	}
}
/*
this function return the last number in the vector and deletes it from the vector
---------------
input
---------------
none
---------------
output
---------------
last number in the vector
*/
int Vector::pop_back()
{
	int element = 0;
	if ((!this->empty())&&this->_size!=0)
	{
		this->_size--;
		element = this->_elements[this->_size];
		this->_elements[this->_size] = NULL;
	}
	else
	{
		element = ERROR;
		cout << "error:: empty vector" << endl;
	}
	return element;
}
/*
this function allocates space in memory for a vector if neede
---------------
input
---------------
wanted new capacity
---------------
output
---------------
none
*/
void Vector::reserve(int n)
{
	int allocation = 0;
	if (this->_capacity <= n)
	{
		allocation = n - this->_capacity;
		allocation /= this->_resizeFactor;//getting how many times resizeFactor needed to add
		if (allocation%this->_resizeFactor != 0)//checking if resizeFactor times are not a double type
		{
			allocation++;
		}
		int *newElements = new int[this->_capacity + (this->_resizeFactor*allocation)];
		for (int i = 0; i < this->_capacity; i++)
		{
			newElements[i] = this->_elements[i];
		}
		this->_capacity = this->_capacity + (this->_resizeFactor*allocation);
		delete[] this->_elements;
		this->_elements = newElements;
	}
}
/*
this function resizes a vector(deletes number if needed or allocates more space if needed)
---------------
input
---------------
new wanted size
---------------
output
---------------
none
*/
void Vector::resize(int n)
{
	if (this->_size <= n)
	{
		for (int i = n; i > this->_size; i--)
		{
			this->_elements[i - 1] = NULL;
		}
		this->_size = n;
	}
	else
	{
		this->reserve(n);
	}
}
void Vector::assign(int val)//this function puts the same number to all spaces in the vector
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
/*
this function same as other resize but this function also puts the same number in all spaces
---------------
input
---------------
new wanted space,new value
---------------
output
---------------
none
*/
void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}
/*
this function return the elemnt at n place
---------------
input
---------------
index
---------------
output
---------------
number at the index
*/
int& Vector::operator[](int n) const
{
	if (n >= this->_size)
	{
		cout << "out of boundries" << endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}
Vector::Vector(const Vector& other)//copy constractor
{
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_size; i++)//copying vector elements
	{
		this->_elements[i] = other[i];
	}
}
Vector& Vector::operator=(const Vector& other)//copy Vector function
{
	Vector *newVector = new Vector(other);
	this->~Vector();
	return *newVector;
}
/* ******this function is just a mistake******
bool flag = true;
if (this->_size = other.size())
{
for (int i = 0; i < this->_size&&flag; i++)
{
if (this->_elements[i] != other[i])
{
flag = false;
}
}
}
else
{
flag = false;
}
return flag;
*/